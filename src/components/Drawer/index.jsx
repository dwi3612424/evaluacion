import React, { useState } from "react";
import { Drawer, Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { Button } from "antd/es/radio";
import { useAuth } from "../../hooks/useAuth";

const DrawerComponent = () => {

    const [open, setOpen] = useState(false);
    const showDrawer = () => {
        setOpen(true);
    }
    const onClose = () => {
        setOpen(false);
    }

    const { user, logout } = useAuth();



    return (
        <>
            <Avatar
                onClick={showDrawer}
                size={44}
                style={{ backgroundColor: '#87d068', cursor: 'pointer' }}
                icon={<UserOutlined />}
            />
            <Drawer title="Basic Drawer" onClose={onClose} open={open}>
                <p style={{color: '#000'}} >{user.readerFound.readername}</p>
                <p style={{color: '#000'}} >{user.readerFound.email}</p>
                <Button onClick={() => logout()} >Cerrar sesión</Button>
            </Drawer>
        </>
    )
}

export default DrawerComponent;




