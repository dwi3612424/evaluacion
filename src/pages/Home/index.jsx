import React from 'react'
import { useAuth } from '../../hooks/useAuth';
import './home.css';
import Nav from '../../components/Nav';

const Home = () => {
  
  const { user, logout } = useAuth();
  console.log( user );
  
  return (
    <>
      <Nav />
      <h1>{user.readerFound.readername}</h1>
      <a href="/"  className='btn' onClick={ logout } >logout</a>
    </>
  )
}

export default Home;