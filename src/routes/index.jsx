import React from 'react'
import { useRoutes } from 'react-router-dom';
import Home from '../pages/Home';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Productos from '../pages/Productos';
import Servicios from '../pages/Servicios';
import Contacto from '../pages/Contacto';

import { useAuth } from '../hooks/useAuth';

const AppRoutes = () => {
  const { user } = useAuth();
  let routes = useRoutes([
    { path: '/', element: user ? <Home /> : <Login /> },
    { path: '/login', element: <Login /> },
    { path: '/register', element: <Register /> },
    { path: '/productos', element: <Productos /> },
    { path: '/servicios', element: <Servicios /> },
    { path: '/contacto', element: <Contacto /> }
  ]);
  return routes;
}

export default AppRoutes;